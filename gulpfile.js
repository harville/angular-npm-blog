(function () {
  'use strict';

  var gulp = require('gulp'),
  sass = require('gulp-sass'),
  minifycss = require('gulp-clean-css'),
  jshintstylish = require('jshint-stylish'),
  jshint = require('gulp-jshint'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  notify = require('gulp-notify'),
  sourcemaps = require('gulp-sourcemaps'),
  del = require('del');

  gulp.task('clean-wipe', function(cb) {
    del([
      'app/dist/css',
      'app/dist/js',
      'app/dist/img',
      ], cb);
  });

  gulp.task('vendor-styles', function() {
    return gulp.src('app/sass/vendor.sass')
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('vendor.min.css'))
    .pipe(minifycss())
    .pipe(gulp.dest('app/dist/css'))
    .pipe(notify({ message: 'Vendor styles compiled' }));
  });

  gulp.task('vendor-scripts', function () {
    return gulp.src([
      'app/bower_components/html5-boilerplate/dist/js/vendor/modernizr-2.8.3.min.js',
      'app/bower_components/jquery/dist/jquery.js',
      'app/bower_components/angular/angular.js',
      'app/bower_components/angular-route/angular-route.js',
      'app/bower_components/foundation-sites/dist/js/foundation.js',
      ], {base: 'app/bower_components/'})
    .pipe(concat('vendor.min.js'))
    .pipe(uglify({mangle: false, compress: {drop_debugger: false}}))
    .pipe(gulp.dest('app/dist/js/'))
    .pipe(notify({ message: 'Vendor scripts compiled' }));
  });

  gulp.task('styles', function() {
    return gulp.src('app/sass/app.sass')
    .pipe(sass({
      includePaths: ['app/bower_components/foundation-sites/scss']
    }).on('error', sass.logError))
    .pipe(concat('app.min.css'))
    .pipe(minifycss())
    .pipe(gulp.dest('app/dist/css'))
    .pipe(notify({ message: 'App styles compiled' }));
  });

  gulp.task('scripts', function () {
    return gulp.src([
      'app/app.js',
      'app/services/*.js',
      'app/filters/*.js',
      'app/components/**/*.js',
      ], {base: 'app/'})
    .pipe(jshint())
    .pipe(jshint.reporter(jshintstylish))
    .pipe(sourcemaps.init())
    .pipe(concat('app.min.js'))
    .pipe(uglify({mangle: false, compress: {drop_debugger: false}}))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('app/dist/js/'))
    .pipe(notify({ message: 'App scripts compiled' }));
  });

  gulp.task('default', [
    'styles',
    'scripts',
  ]);

  gulp.task('watch', function() {
    gulp.start('default');
    gulp.watch('app/**/*.sass', ['styles']);
    gulp.watch('app/**/*.js', ['scripts']);
  });

  gulp.task('deploy', [
    'clean-wipe',
    'vendor-styles',
    'vendor-scripts',
    'styles',
    'scripts',
  ]);

}());
