
# ATLASSIAN FRONT-END MINI-PROJECT

## What do I need to do?

Please build a simple website based on the image and json provided. The point of this mini-project is to get an idea of how you approach html/css/js. Go ahead and slice the images from the png. The content is in the included json file. Use any client-side js library you prefer to parse and display the data. 

## Basic Requirements

- Use the included json file to build out the page.
— Each ‘track’ should have its own tab.
- The session titles make up the tabs on the left.

## Install

    git clone git@bitbucket.org:harville/angular-npm-blog.git
    cd angular-npm-blog
    npm install

I typically gitignore generated assets, but I didn't do that here for the sake of simplicity. If the page is not styled for some reason, issue `gulp deploy` at project root.

## Serve

    npm start

Navigate to `http://localhost:8022`. If you need to change the port, please do so in package.json.

## Discussion

I chose to use the technologies specifically listed in the job posting: Javascript, AngularJS, SASS, Git, and NPM. AngularJS is an older framework, so I turned back the clock a bit to set that up. I started from the base project `angular/angular-seed`. I then added gulp.js to compile the SASS down to minified CSS, bundle the Javascript into minified vendor and app files, and facilitate a quicker development workflow. You mentioned in the job posting that responsive views are important, so I'm using the Foundation framework to quickly achieve that.