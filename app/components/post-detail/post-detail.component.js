(function () {
  'use strict';

  function PostDetailController($scope) {
    var ctrl = this;
  }

  angular.module('blog')
  .component('postDetail', {
    templateUrl: 'components/post-detail/post-detail.html',
    bindings: {
      post: '<'
    },
    controller: PostDetailController
  });

}());
