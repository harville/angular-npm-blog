(function () {
  'use strict';

  function PostTabsController($scope) {
    var ctrl = this;

    ctrl.setCurrentTab = function(tab) {
      // call posts component update method
      ctrl.updateCurrentTab({tab: tab});
    };
  }

  angular.module('blog')
  .component('postTabs', {
    templateUrl: 'components/post-tabs/post-tabs.html',
    bindings: {
      tabs: '<',
      updateCurrentTab: '&'
    },
    controller: PostTabsController
  });

}());
