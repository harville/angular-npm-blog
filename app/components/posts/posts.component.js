(function () {
  'use strict';

  var PostsApi = {
    all: {},
    posts: {},
    tabs: [],
    currentTab: '',
    currentPost: {},

    setPosts: function(posts) {
      this.posts = posts;
    },

    getPosts: function() {
      return ! (0 === Object.keys(this.posts).length && Object === this.posts.constructor) ? this.posts : false;
    },

    setTabs: function(tabs) {
      this.tabs = tabs;
    },

    getTabs: function() {
      return (this.tabs.length) ? this.tabs : false;
    },

    setCurrentTab: function(tab) {
      this.currentTab = tab;
    },

    getCurrentTab: function() {
      return (this.currentTab.length) ? this.currentTab : false;
    },

    setCurrentPost: function(post) {
      this.currentPost = post;
    },

    getCurrentPost: function() {
      return ! (0 === Object.keys(this.currentPost).length && Object === this.currentPost.constructor) ? this.currentPost : false;
    },

    // pull tab names from posts
    pluckTabs: function() {
      var allTabs = this.all.map(function(post) {
        return post.Track.Title;
      });

      var uniqueTabs = allTabs.filter(function(value, index, self) {
        return self.indexOf(value) === index;
      });

      return uniqueTabs;
    },

    // filter posts for specified tab
    filterPosts: function(currentTab) {
      var posts = this.all.filter(function(value, index, self) {
        var postTrack = ((value.Track || {}).Title || '');
        return postTrack === currentTab;
      });

      return posts
    },

    deriveTabProperties: function() {
      var tabs = this.pluckTabs();
      this.setTabs(tabs);

      if (! this.getCurrentTab()) {
        this.setCurrentTab(tabs[0]);
      }
    },

    derivePostProperties: function() {
      var posts = this.filterPosts(this.currentTab);
      this.setPosts(posts);

      if (! this.getCurrentPost()) {
        this.setCurrentPost(posts[0]);
      }
    },

    // respond to tab click
    updateCurrentTab: function(tab) {
      this.setCurrentTab(tab);

      var posts = this.filterPosts(tab);
      this.setPosts(posts);

      this.setCurrentPost(posts[0]);
    },

    // respond to post click
    updateCurrentPost: function(post) {
      this.setCurrentPost(post);
    },

    // import post data; expose methods to retreive data
    resolve: function(http) {
      var api = {};

      // cull from `http.data.Items`
      if ('object' === typeof http) {
        this.all = ((http.data || {}).Items || []);

        this.deriveTabProperties();
        this.derivePostProperties();

        var _this = this;

        // expose public api methods
        api = {
          getTabs: function() {return _this.getTabs()},
          getCurrentTab: function() {return _this.getCurrentTab()},
          getPosts: function() {return _this.getPosts()},
          getCurrentPost: function() {return _this.getCurrentPost()},
          updateCurrentTab: function(tab) {return _this.updateCurrentTab(tab)},
          updateCurrentPost: function(post) {return _this.updateCurrentPost(post)},
        };
      }

      return api;
    }
  };

  function PostsController($scope, FetchPostsService) {
    var ctrl = this;

    // pull latest from acting source of truth: the PostsApi
    ctrl.pullLatestFromStore = function() {
      // expose public parameters for child components
      ctrl.tabs = ctrl.api.getTabs();
      ctrl.currentTab = ctrl.api.getCurrentTab();
      ctrl.posts = ctrl.api.getPosts();
      ctrl.currentPost = ctrl.api.getCurrentPost();
    }

    // fetch, process, then save posts to a store accessed via api
    FetchPostsService.then(function(http) {
      ctrl.api = PostsApi.resolve(http);
      ctrl.pullLatestFromStore();
    });

    ctrl.updateCurrentTab = function(tab) {
      ctrl.api.updateCurrentTab(tab);
      ctrl.pullLatestFromStore();
    };

    ctrl.updateCurrentPost = function(post) {
      ctrl.api.updateCurrentPost(post);
      ctrl.pullLatestFromStore();
    };
  }
  PostsController.$inject = ['$scope', 'FetchPostsService'];

  angular.module('blog')
  .component('posts', {
    templateUrl: 'components/posts/posts.html',
    controller: PostsController
  });

}());
