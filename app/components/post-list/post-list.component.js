(function () {
  'use strict';

  function PostListController($scope) {
    var ctrl = this;

    ctrl.setCurrentPost = function(post) {
      // call posts component update method
      ctrl.updateCurrentPost({post: post});
    }
  }

  angular.module('blog')
  .component('postList', {
    templateUrl: 'components/post-list/post-list.html',
    bindings: {
      posts: '<',
      currentTab: '<',
      updateCurrentPost: '&'
    },
    controller: PostListController
  });

}());
