(function () {
  'use strict';

  angular.module('blog').factory('FetchPostsService', ['$http', function($http) {
    return $http.get('data/sessions.json')
    .then(function(data) {
      return data;
    }, function(error) {
      return error;
    });
  }]);

}());
